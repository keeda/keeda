from urllib2 import urlopen
from HTMLParser import HTMLParser
from queue import KeedaQueue
from bloomset import BloomSet
from threading import Thread
import utilities

class KeedaWorker(HTMLParser):

	def __init__(self, queue, bloomset):
		HTMLParser.__init__(self)
		self.queue = queue
		self.bloomset = bloomset
		self.urls = []
		self.currentUrl = ''
		t = Thread(target=self.crawl)
		t.daemon = True
		t.start()


	def handle_starttag(self, tag, attrs):
		temp = ''
		if tag == 'a':
			for (attribute, value) in attrs:
				if attribute == 'href':
					if utilities.validateURL(value):
						temp = value
					else:
						temp = (utilities.joinURL(self.currentUrl,value))
					try:
						temp = temp.decode('utf-8')
					except:
						return
					if not self.bloomset.get(temp):
						self.bloomset.add(temp)
						self.urls.append(temp)

	def crawl(self):
		while True:
			self.urls = []
			url = self.queue.get()
			self.currentUrl=url
			try:
				res = urlopen(url)
				body = res.read().decode("ISO-8859-1")
				self.feed(body)
				print url,len(self.urls)
				self.queue.put(self.urls)
			except Exception:
				pass
			finally:
				self.queue.task_done()
