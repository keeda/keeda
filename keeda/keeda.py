from worker import KeedaWorker
from bloomset import BloomSet
from queue import KeedaQueue
import utilities

class Keeda:
	def __init__(self , baseUrl, numthreads=1):
			self.baseUrl = baseUrl
			self.numthreads = numthreads
			if not utilities.validateURL(baseUrl):
				raise ValueError("ayuhi ")

	def start(self):
		keedaQueue = KeedaQueue(self.baseUrl, utilities.getDomain(self.baseUrl))
		bloomSet = BloomSet(utilities.getDomain(self.baseUrl))
		for i in range(self.numthreads):
			KeedaWorker(keedaQueue,bloomSet)
		keedaQueue.join()
		keedaQueue.close()
		bloomset.close()
